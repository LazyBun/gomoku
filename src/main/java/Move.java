class Move {
    int x;
    int y;

    int moveValue;

    public Move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "x: " + x + " | y:" + y + " | value:" + moveValue;
    }
}