public class GomokuState {

    public static int NONE = 0;
    public static int BLACK = 1;
    public static int WHITE = 2;

    int turn = WHITE;
    int[][] board;
    int size = 19;

    public GomokuState(int size) {
        this.board = new int[size][size];
        this.size = size;
        initializeBoard(this.board);
    }

    //Copying constructor
    public GomokuState(GomokuState orig) {
        this.size = orig.size;
        this.board = new int[size][size];
        for (int i = 0; i < size; i++) {
            System.arraycopy(orig.board[i], 0, board[i], 0, size);
        }
        this.turn = orig.turn;
    }

    public void initializeBoard(int[][] board) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                board[i][j] = NONE;
            }
        }
    }


    public int getWinner() {
        int winner = NONE;

        winner = horizontalWinner();
        if (winner != NONE) return winner;

        winner = verticalWinner();
        if (winner != NONE) return winner;

        winner = diagonalWinner();
        if (winner != NONE) return winner;

        return winner;
    }

    private int diagonalWinner() {
        int[][] shiftedBoard = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (j + i < size) shiftedBoard[i][j] = board[i][j + i];
                else shiftedBoard[i][j] = board[i][j];
            }
        }
        int winner = NONE;
        for (int i = 0; i < size; i++) {
            int count = 0;
            int who = NONE;
            for (int j = 0; j < size; j++) {
                if (shiftedBoard[j][i] == NONE) {
                    count = 0;
                    continue;
                }
                if (shiftedBoard[j][i] == who) {
                    count++;
                    if(count == 5) break;
                } else {
                    count = 1;
                    who = shiftedBoard[j][i];
                }
            }
            if(count == 5) {
                winner = who;
                break;
            }
        }
        if (winner == NONE) {
            shiftedBoard = new int[size][size];
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (j-i >= 0) shiftedBoard[i][j] = board[i][j - i];
                    else shiftedBoard[i][j] = board[i][(size - 1) + (j-i)];
                }
            }
            for (int i = 0; i < size; i++) {
                int count = 0;
                int who = NONE;
                for (int j = 0; j < size; j++) {
                    if (shiftedBoard[j][i] == NONE) {
                        count = 0;
                        continue;
                    }
                    if (shiftedBoard[j][i] == who) {
                        count++;
                        if(count == 5) break;
                    } else {
                        count = 1;
                        who = shiftedBoard[j][i];
                    }
                }
                if(count == 5) {
                    winner = who;
                    break;
                }
            }
        }
        return winner;

    }

    private int verticalWinner() {
        int winner = NONE;
        for (int i = 0; i < size; i++) {
            int count = 0;
            int who = NONE;
            for (int j = 0; j < size; j++) {
                if (board[j][i] == NONE) {
                    count = 0;
                    continue;
                }
                if (board[j][i] == who) {
                    count++;
                    if(count == 5) break;
                } else {
                    count = 1;
                    who = board[j][i];
                }
            }
            if(count == 5) {
                winner = who;
                break;
            }
        }
        return winner;
    }

    private int horizontalWinner() {
        int winner = NONE;
        for (int i = 0; i < size; i++) {
            int count = 0;
            int who = NONE;
            for (int j = 0; j < size; j++) {
                if (board[i][j] == NONE) {
                    count = 0;
                    continue;
                }
                if (board[i][j] == who) {
                    count++;
                    if(count == 5) break;
                } else {
                    count = 1;
                    who = board[i][j];
                }
            }
            if(count == 5) {
                winner = who;
                break;
            }
        }
        return winner;
    }

    public int getPieceAt(int x, int y) {
        if (x < size && x >= 0 && y < size && y >= 0) {
            return board[x][y];
        } else {
            System.out.println("Out of bounds: x:" + x + " | y:" + y );
            return NONE;
        }
    }

    public void playPieceAtAndChangeTurn(int x, int y) {
        board[x][y] = turn;
        turn = turn == WHITE ? BLACK : WHITE;
    }
}
