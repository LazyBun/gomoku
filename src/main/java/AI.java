import java.time.Instant;

public class AI {

    static int maxDepth = 4;
    static int currentDepth = 4;
    int turn = 0;
    int color;
    GomokuState currentGameState;
    boolean firstMove = true;

    int weightMe = 1;
    int weightHim = 1;

    public AI(int color, GomokuState currentGameState) {
        this.color = color;
        this.currentGameState = currentGameState;
    }

    public AI(int color, GomokuState currentGameState, int weightMe, int weightHim) {
        this.weightMe = weightMe;
        this.weightHim = weightHim;
        this.color = color;
        this.currentGameState = currentGameState;
    }

    //NO-ALPHA-BETA
//    public Move makeMove() {
//        if (firstMove) {
//            firstMove = false;
//            if (currentGameState.board[currentGameState.size/2][currentGameState.size/2] == GomokuState.NONE) return new Move(currentGameState.size/2, currentGameState.size/2);
//            else return new Move(currentGameState.size/2+1, currentGameState.size/2+1);
//        }
//        Instant now1 = Instant.now();
//        StateTree tree = new StateTree(currentGameState, 0, color);
//        System.out.print("Generating tree took: ");
//        System.out.println(Instant.now().toEpochMilli() - now1.toEpochMilli());
//        Instant now = Instant.now();
//        Move move = miniMax(tree, true);
//        System.out.print("Decision took: ");
//        System.out.println(Instant.now().toEpochMilli() - now.toEpochMilli());
//        return move;
//    }
//
//    public Move miniMax(StateTree tree, boolean isMax) {
//        if(tree.children.isEmpty()) {
//            tree.moveMade.moveValue = tree.evaluateState();
//            return tree.moveMade;
//        } else {
//            Move bestMove = null;
//            for (int i = 0; i < tree.children.size(); i++) {
//                Move move = miniMax(tree.children.get(i), !isMax);
//
//                if (bestMove != null) {
//                    if (isMax) {
//                        if (bestMove.moveValue < move.moveValue) {
//                            bestMove = move;
//                        }
//                    } else {
//                        if (bestMove.moveValue > move.moveValue) {
//                            bestMove = move;
//                        }
//                    }
//                } else {
//                    bestMove = move;
//                }
//
//            }
//            if (bestMove != null && tree.moveMade != null) {
//                bestMove.x = tree.moveMade.x;
//                bestMove.y = tree.moveMade.y;
//            } else {
//                System.out.println(bestMove);
//            }
//            return bestMove;
//        }
//    }
    //NO-ALPHA-BETA

    //ALPHA-BETA
    public Move makeMove() {
        if (firstMove) {
            firstMove = false;
            if (currentGameState.board[currentGameState.size/2][currentGameState.size/2] == GomokuState.NONE) return new Move(currentGameState.size/2, currentGameState.size/2);
            else return new Move(currentGameState.size/2+1, currentGameState.size/2+1);
        }
        turn ++;
        if (maxDepth != currentDepth && turn % 20 == 0) { currentDepth += 2; }
        System.out.println("Thinking... (depth: " + currentDepth + ")" );
        StateTreeAlphaBeta tree = new StateTreeAlphaBeta(currentGameState, 0, color);
        Instant now = Instant.now();
        Move move = miniMax(tree, true, Integer.MIN_VALUE, Integer.MAX_VALUE);
        System.out.print("Decision took: ");
        System.out.println(Instant.now().toEpochMilli() - now.toEpochMilli());
        return move;
    }

    public Move miniMax(StateTreeAlphaBeta tree, boolean isMax, int alpha, int beta) {
        StateTreeAlphaBeta child = tree.possibleChild();
        if(child == null) {
            tree.moveMade.moveValue = tree.evaluateState(weightMe, weightHim);
            return tree.moveMade;
        } else {
            Move bestMove = null;
            do {
                if (alpha >= beta) {
                    break;
                }
                Move move = miniMax(child, !isMax, alpha, beta);

                if (bestMove != null) {
                    if (isMax) {
                        if (bestMove.moveValue < move.moveValue) {
                            bestMove = move;
                            alpha = move.moveValue;
                        }
                    } else {
                        if (bestMove.moveValue > move.moveValue) {
                            bestMove = move;
                            beta = move.moveValue;
                        }
                    }
                } else {
                    bestMove = move;
                    if (isMax) {
                        alpha = move.moveValue;
                    } else {
                        beta = move.moveValue;
                    }
                }

                child = tree.possibleChild();
            } while (child != null);

            if (bestMove != null && tree.moveMade != null) {
                bestMove.x = tree.moveMade.x;
                bestMove.y = tree.moveMade.y;
            } else {
                System.out.println(bestMove);
            }
            return bestMove;
        }
    }
    //ALPHA-BETA
}
