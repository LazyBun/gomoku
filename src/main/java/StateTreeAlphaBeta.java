import java.util.*;

public class StateTreeAlphaBeta {

    //AI's color
    int color;

    int depth;

    Move moveMade;

    GomokuState state;

    List<StateTreeAlphaBeta> children = new ArrayList<>();


    Random rnd = new Random();

    public StateTreeAlphaBeta(GomokuState state, int depth, int color) {
        this.state = state;
        this.depth = depth;
        this.color = color;
    }

    //DUMB PICK
//    public StateTreeAlphaBeta possibleChild() {
//        if (depth < AI.currentDepth) {
//            for (int i = 0; i < state.size; i++) {
//                for (int j = 0; j < state.size; j++) {
//                    if (state.board[i][j] == GomokuState.NONE) {
//                        int finalI = i;
//                        int finalJ = j;
//                        if (children.stream().noneMatch(it -> (it.moveMade.x == finalI && it.moveMade.y == finalJ))) {
//                            GomokuState copy = new GomokuState(state);
//                            copy.playPieceAtAndChangeTurn(i, j);
//                            StateTreeAlphaBeta child = new StateTreeAlphaBeta(copy, depth + 1, color);
//                            child.moveMade = new Move(i, j);
//                            children.add(child);
//                            return child;
//                        }
//                    }
//                }
//            }
//        }
//        return null;
//    }

    //NEAR PICK
    public StateTreeAlphaBeta possibleChild() {
        if (depth < AI.currentDepth) {
            for (int i = 0; i < state.size; i++) {
                for (int j = 0; j < state.size; j++) {
                    if (state.board[i][j] == GomokuState.NONE) {
                        int finalI = i;
                        int finalJ = j;
                        if (children.stream().noneMatch(it -> (it.moveMade.x == finalI && it.moveMade.y == finalJ)) &&
                                thereIsSomethingAround(i, j)) {
                            GomokuState copy = new GomokuState(state);
                            copy.playPieceAtAndChangeTurn(i, j);
                            StateTreeAlphaBeta child = new StateTreeAlphaBeta(copy, depth + 1, color);
                            child.moveMade = new Move(i, j);
                            children.add(child);
                            return child;
                        }
                    }
                }
            }
        }
        return null;
    }

    private boolean thereIsSomethingAround(int x, int y) {
        for (int i = x-1; i <= x+1; i++) {
            for (int j = y-1; j <= y+1; j++) {
                if (i < 0 || j < 0 || j == state.size || i == state.size) continue;
                if (state.board[i][j] != GomokuState.NONE) return true;
            }
        }
        return false;
    }

    public int evaluateState(int weightMe, int weightHim) {
        return simplyCheck(weightMe,weightHim);
    }

    public int evaluateState() {
        return simplyCheck(1,1);
    }

    public int randomEval() {
        return rnd.nextInt(12345);
    }

    public int simplyCheck(int weightMe, int weightHim) {
        int me2InRow = 0;
        int me3InRow = 0;
        int me4InRow = 0;
        int me5InRow = 0;
        int him2InRow = 0;
        int him3InRow = 0;
        int him4InRow = 0;
        int him5InRow = 0;

        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (state.board[i][j] == who) {
                    inRow ++;
                } else {
                    who = state.board[i][j];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 2) me2InRow++;
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 2) him2InRow++;
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }
        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (state.board[j][i] == who) {
                    inRow ++;
                } else {
                    who = state.board[j][i];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 2) me2InRow++;
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 2) him2InRow++;
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }


        int[][] shiftedBoard = new int[state.size][state.size];
        for (int i = 0; i < state.size; i++) {
            for (int j = 0; j < state.size; j++) {
                if (j + i < state.size) shiftedBoard[i][j] = state.board[i][j + i];
                else shiftedBoard[i][j] = state.board[i][j];
            }
        }
        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (shiftedBoard[j][i] == who) {
                    inRow ++;
                } else {
                    who = shiftedBoard[j][i];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 2) me2InRow++;
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 2) him2InRow++;
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }
        shiftedBoard = new int[state.size][state.size];
        for (int i = 0; i < state.size; i++) {
            for (int j = 0; j < state.size; j++) {
                if (j-i >= 0) shiftedBoard[i][j] = state.board[i][j - i];
                else shiftedBoard[i][j] = state.board[i][(state.size - 1) + (j-i)];
            }
        }
        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (shiftedBoard[j][i] == who) {
                    inRow ++;
                } else {
                    who = shiftedBoard[j][i];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 2) me2InRow++;
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 2) him2InRow++;
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }

        return (((him5InRow * -10000) + (him4InRow * -700) + (him3InRow * -150) + (-10 * him2InRow)) * weightHim) +
                (((me2InRow * 10) + (me3InRow * 150) + (me4InRow * 700) + (me5InRow * 10000)) * weightMe);
    }

}
