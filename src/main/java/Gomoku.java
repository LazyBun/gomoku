import javax.swing.*;

public class Gomoku
{

    public static void main(String[] args) {

        //Board size
        int size = 7;

        if (args.length > 0)
            size = Integer.parseInt(args[0]);

        JFrame frame = new JFrame();

        final int FRAME_WIDTH = 600;
        final int FRAME_HEIGHT = 650;
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setTitle("Gomoku");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GomokuPanel panel = new GomokuPanel(size);
        frame.add(panel);

        frame.setVisible(true);
    }
}

