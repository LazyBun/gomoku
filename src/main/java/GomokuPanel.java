import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

class GomokuPanel extends JPanel
{
    private final int MARGIN = 5;
    private final double PIECE_FRAC = 0.9;

    private int size = 19;
    private GomokuState state;

    private AI ai1;
    private AI ai2;
    private int playerColor;
    private boolean thinking = false;

    public GomokuPanel()
    {
        this(19);
    }

    public GomokuPanel(int size)
    {
        super();
        this.size = size;
        state = new GomokuState(size);
        addMouseListener(new GomokuListener());

        //Comment out White to ai vs ai
        playerColor = GomokuState.NONE;
//        playerColor = GomokuState.WHITE;

        ai1 = new AI(GomokuState.BLACK, state, 5, 5);
        ai2 = new AI(GomokuState.WHITE, state, 1, 1);
    }

    class GomokuListener extends MouseAdapter
    {
        public void mouseReleased(MouseEvent e)
        {
            int row = -1;
            int col = -1;
            if (state.turn == playerColor) {
                double panelWidth = getWidth();
                double panelHeight = getHeight();
                double boardWidth = Math.min(panelWidth, panelHeight) - 2 * MARGIN;
                double squareWidth = boardWidth / size;
                double pieceDiameter = PIECE_FRAC * squareWidth;
                double xLeft = (panelWidth - boardWidth) / 2 + MARGIN;
                double yTop = (panelHeight - boardWidth) / 2 + MARGIN;
                col = (int) Math.round((e.getX() - xLeft) / squareWidth - 0.5);
                row = (int) Math.round((e.getY() - yTop) / squareWidth - 0.5);
            } else if(state.turn == ai1.color) {
                Move move = ai1.makeMove();
                row = move.x;
                col = move.y;
            } else if(state.turn == ai2.color) {
                Move move = ai2.makeMove();
                row = move.x;
                col = move.y;
            }

            if (row >= 0 && row < size && col >= 0 && col < size
                    && state.getPieceAt(row, col) == GomokuState.NONE
                    && state.getWinner() == GomokuState.NONE) {
                state.playPieceAtAndChangeTurn(row, col);
                repaint();
                int winner = state.getWinner();
                if (winner != GomokuState.NONE)
                    JOptionPane.showMessageDialog(null,
                            (winner == GomokuState.BLACK) ? "Black wins!"
                                    : "White wins!");

                if (ai1 != null) ai1.currentGameState = state;
                if (ai2 != null) ai2.currentGameState = state;
            }
        }
    }


    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        double panelWidth = getWidth();
        double panelHeight = getHeight();

        g2.setColor(new Color(0.925f, 0.670f, 0.34f)); // light wood
        g2.fill(new Rectangle2D.Double(0, 0, panelWidth, panelHeight));


        double boardWidth = Math.min(panelWidth, panelHeight) - 2 * MARGIN;
        double squareWidth = boardWidth / size;
        double gridWidth = (size - 1) * squareWidth;
        double pieceDiameter = PIECE_FRAC * squareWidth;
        boardWidth -= pieceDiameter;
        double xLeft = (panelWidth - boardWidth) / 2 + MARGIN;
        double yTop = (panelHeight - boardWidth) / 2 + MARGIN;

        g2.setColor(Color.BLACK);
        for (int i = 0; i < size; i++) {
            double offset = i * squareWidth;
            g2.draw(new Line2D.Double(xLeft, yTop + offset,
                    xLeft + gridWidth, yTop + offset));
            g2.draw(new Line2D.Double(xLeft + offset, yTop,
                    xLeft + offset, yTop + gridWidth));
        }

        for (int row = 0; row < size; row++)
            for (int col = 0; col < size; col++) {
                int piece = state.getPieceAt(row, col);
                if (piece != GomokuState.NONE) {
                    Color c = (piece == GomokuState.BLACK) ? Color.BLACK : Color.WHITE;
                    g2.setColor(c);
                    double xCenter = xLeft + col * squareWidth;
                    double yCenter = yTop + row * squareWidth;
                    Ellipse2D.Double circle
                            = new Ellipse2D.Double(xCenter - pieceDiameter / 2,
                            yCenter - pieceDiameter / 2,
                            pieceDiameter,
                            pieceDiameter);
                    g2.fill(circle);
                    g2.setColor(Color.black);
                    g2.draw(circle);
                }
            }
    }
}