import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StateTree {

    int color;
    int depth;

    Move moveMade;

    GomokuState state;

    List<StateTree> children = new ArrayList<>();

    Random rnd = new Random();

    public StateTree(GomokuState state, int depth, int color) {
        this.state = state;
        this.depth = depth;
        this.color = color;
        if (depth < AI.currentDepth) generateChildren();
    }


    private void generateChildren() {
        for (int i = 0; i < state.size; i++) {
            for (int j = 0; j < state.size; j++) {
                if (state.board[i][j] == GomokuState.NONE) {
                    GomokuState copy = new GomokuState(state);
                    copy.playPieceAtAndChangeTurn(i, j);
                    StateTree child = new StateTree(copy, depth + 1, color);
                    child.moveMade = new Move(i, j);
                    children.add(child);
                }
            }
        }
    }


    public int evaluateState() {
        return simplyCheck();
    }

    public int surroundingEval() {
        int score = 0;
        for (int i = 0; i < state.size; i++) {
            for (int j = 0; j < state.size; j++) {
                int mine = 0;
                int theirs = 0;
                for (int ii = i - 4; ii < i + 4; ii++) {
                    for (int jj = j - 4; jj < j + 4; jj++) {
                        if (ii < 0 || ii >= state.size || jj < 0 || jj >= state.size) break;
                        if (state.board[ii][jj] == GomokuState.NONE) {}
                        else if (state.board[ii][jj] == color) mine++;
                        else theirs++;
                    }
                }
                score += (theirs * theirs) + mine;
            }
        }
        return score;
    }

    public int randomEval() {
        return rnd.nextInt(12345);
    }

    public int simplyCheck() {
        int me3InRow = 0;
        int me4InRow = 0;
        int me5InRow = 0;
        int him3InRow = 0;
        int him4InRow = 0;
        int him5InRow = 0;

        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (state.board[i][j] == who) {
                    inRow ++;
                } else {
                    who = state.board[i][j];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }
        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (state.board[j][i] == who) {
                    inRow ++;
                } else {
                    who = state.board[j][i];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }
        int[][] shiftedBoard = new int[state.size][state.size];
        for (int i = 0; i < state.size; i++) {
            for (int j = 0; j < state.size; j++) {
                if (j + i < state.size) shiftedBoard[i][j] = state.board[i][j + i];
                else shiftedBoard[i][j] = state.board[i][j];
            }
        }
        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (shiftedBoard[j][i] == who) {
                    inRow ++;
                } else {
                    who = shiftedBoard[j][i];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }

        shiftedBoard = new int[state.size][state.size];
        for (int i = 0; i < state.size; i++) {
            for (int j = 0; j < state.size; j++) {
                if (j-i >= 0) shiftedBoard[i][j] = state.board[i][j - i];
                else shiftedBoard[i][j] = state.board[i][(state.size - 1) + (j-i)];
            }
        }
        for (int i = 0; i < state.size; i++) {
            int inRow = 0;
            int who = GomokuState.NONE;
            for (int j = 0; j < state.size; j++) {
                if (shiftedBoard[j][i] == who) {
                    inRow ++;
                } else {
                    who = shiftedBoard[j][i];
                    inRow = 1;
                }
                if (who == color) {
                    //good
                    if (inRow == 3) me3InRow++;
                    if (inRow == 4) me4InRow++;
                    if (inRow == 5) me5InRow++;
                } else if (who != GomokuState.NONE) {
                    //bad
                    if (inRow == 3) him3InRow++;
                    if (inRow == 4) him4InRow++;
                    if (inRow == 5) him5InRow++;
                }
            }
        }

        return him5InRow * -10000 + him4InRow * -500 + him3InRow * -100 + me3InRow * 150 + me4InRow * 700 + me5InRow * 10000;
    }

}
